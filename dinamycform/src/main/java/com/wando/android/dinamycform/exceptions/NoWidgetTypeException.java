package com.wando.android.dinamycform.exceptions;

public class NoWidgetTypeException extends Exception {
    public NoWidgetTypeException(String message) {
        super(message);
    }
}