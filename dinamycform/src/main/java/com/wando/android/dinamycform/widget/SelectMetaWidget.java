package com.wando.android.dinamycform.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.generics.DFWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 23/02/2018.
 */

public class SelectMetaWidget extends DFWidget {

    transient private List<ItemComboField> itemOptions = new ArrayList<>();
    transient private ItemComboField itemSelected = null;
    transient private String formatItems = ""; //"[{\"key\":\"1\",\"value\":\"option 1\"},{\"key\":\"2\",\"value\":\"option 2\"},{\"key\":\"3\",\"value\":\"option 3\"}]";
    transient Context mContext;

    //@Override
    public View generateView(@NonNull final Context context) {
        this.mContext = context;
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        Log.i("INFO"," verboseName "+ verboseName);
        //Label
        TextView tv = new TextView(context);
        tv.setText(verboseName);
        //tv.setTextSize(10);
        //tv.setTypeface(null, Typeface.BOLD);
        //tv.setTextColor(0xFF555555);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        Spinner spinner = new Spinner(context);
        spinner.setPadding(5, 10, 5, 10);
        if (formatItems != null) {
            try {
                JSONArray jsonArray = new JSONArray(formatItems);
                for (int x = 0; x < jsonArray.length(); x++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    ItemComboField item = new Gson().fromJson(jsonObject.toString(), ItemComboField.class);
                    itemOptions.add(item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ArrayAdapter<ItemComboField> spinnerData =
                new ArrayAdapter<>(context, R.layout.custom_simple_spinner_item, itemOptions);
        spinner.setAdapter(spinnerData);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelected = itemOptions.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        linearLayout.addView(spinner);

        return linearLayout;
    }

    public Object getValue() {
        return getItemSelected().key;
    }

    public void setValue(Object object){

    }

    private ItemComboField getItemSelected() {
        return itemSelected;
    }

    @Override
    public void load(JSONObject jsonField) throws JSONException {
        this.verboseName = jsonField.getString("verbose_name");
        this.formatItems = jsonField.getString("choices");
    }

    public class ItemComboField {
        public String key;
        public String value;

        @Override
        public String toString() {
            return value;
        }
    }

}