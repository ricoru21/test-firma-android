package com.wando.android.dinamycform.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by Ricoru on 12/07/17.
 */

public class PermisosUtil {

    public static final int REQUEST_CODE_READ_PERMISSION = 22;
    public static final int PERMISSION_CAMERA = 85;
    public static final int PERMISSION_SMS = 45;
    public static final int PERMISSION_LOCATION = 50;
    public static final int PERMISSION_ALL = 100;

    public static boolean hasRealExternalPermission(Activity activity) {
        return ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void askForGalleryPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE_READ_PERMISSION);
    }

    public static boolean hasCameraPermission(Activity activity) {
        return ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void askForCameraPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_CAMERA);
    }


}
