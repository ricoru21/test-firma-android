package com.wando.android.dinamycform.exceptions;

public class NoElementTypeException extends Exception {
    public NoElementTypeException(String message) {
        super(message);
    }
}