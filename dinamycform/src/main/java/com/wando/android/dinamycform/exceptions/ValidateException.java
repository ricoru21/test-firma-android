package com.wando.android.dinamycform.exceptions;

public class ValidateException extends Exception {
    public ValidateException(String message) {
        super(message);
    }
}