package com.wando.android.dinamycform.elements;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wando.android.dinamycform.DynamicForm;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;
import com.wando.android.dinamycform.generics.DFFieldType;
import com.wando.android.dinamycform.util.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MultipleChoiceFieldType extends DFFieldType {

    transient Context mContext;
    transient String mValue = "";
    transient private HashMap<String, String> arrChoices = new HashMap<>();

    @Override
    public void load(JSONObject jsonDefault, DynamicForm form)
            throws JSONException, NoWidgetTypeException, NoElementTypeException {
        super.load(jsonDefault, form);
        JSONArray jArrchoises = jsonDefault.getJSONObject("field").optJSONArray("choices");
        if (jArrchoises != null) {
            for (int x = 0; x < jArrchoises.length(); x++) {
                JSONObject jsonObject = jArrchoises.getJSONObject(x);
                this.arrChoices.put(jsonObject.getString("key"),
                        jsonObject.getString("value"));
            }
        }
    }

    @Override
    public Object getValue() {
        return getDfWidget().getValue();
    }

    @Override
    public void setValue(Object value) {
        if (this.arrChoices != null) {
            ArrayList<String> itemValues = (ArrayList<String>) value;
            List<String> select_item = new ArrayList<>();
            for (String item : itemValues) {
                select_item.add(this.arrChoices.get(item));
            }
            mValue = TextUtils.join(",", select_item.toArray());
        } else {
            mValue = String.valueOf(value);
        }
    }

    @Override
    public View generateReadOnlyView(@NonNull Context context, ViewGroup root) {
        this.mContext = context;
        LinearLayout.LayoutParams llp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout linearLayoutRoot = new LinearLayout(context);
        linearLayoutRoot.setLayoutParams(llp);
        linearLayoutRoot.setOrientation(LinearLayout.VERTICAL);

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        //Label
        TextView tv = new TextView(context);
        tv.setText(StringUtil.strUpperFirstLetter(this.getVerboseName()));
        tv.setTextSize(15f);
        tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
        LinearLayout.LayoutParams params
                = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 2;
        params.gravity = Gravity.CENTER_VERTICAL;
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        TextView tv2 = new TextView(context);
        tv2.setText(mValue);
        tv2.setTextSize(15f);
        LinearLayout.LayoutParams params2 =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.weight = 1;
        tv2.setLayoutParams(params2);
        linearLayout.addView(tv2);

        linearLayoutRoot.addView(linearLayout);

        View divider = new View(context);
        divider.setBackgroundResource(R.color.colorF6F6);
        LinearLayout.LayoutParams dividerParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        dividerParams.height = 1;
        dividerParams.topMargin = 20;
        dividerParams.bottomMargin = 20;
        divider.setLayoutParams(dividerParams);
        linearLayoutRoot.addView(divider);

        if (TextUtils.isEmpty(mValue)) linearLayoutRoot.setVisibility(View.GONE);

        return linearLayoutRoot;
    }

}
