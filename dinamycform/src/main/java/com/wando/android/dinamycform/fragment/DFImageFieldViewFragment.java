package com.wando.android.dinamycform.fragment;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wando.android.dinamycform.R;

public class DFImageFieldViewFragment extends DialogFragment {

    private static final String ARG_IMAGE = "ARG_PHOTO";
    private static final String ARG_POSITION = "ARG_POSITION";
    private static final String ARG_BOOLEAN = "ARG_BOOLEAN";

    private ImageView img_view;
    private TextView action_cancel;
    private TextView action_eliminar;

    private String mImage;
    private int mPosition;

    public static OnImageDialogFragmentListener mListener;

    public DFImageFieldViewFragment() {
        // Required empty public constructor
    }

    public static DFImageFieldViewFragment newInstance(String mImage, int position, Boolean readOnly) {
        DFImageFieldViewFragment fragment = new DFImageFieldViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE, mImage);
        args.putInt(ARG_POSITION, position);
        args.putBoolean(ARG_BOOLEAN, readOnly);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_dialog, container, false);
        img_view = (ImageView) view.findViewById(R.id.img_view);
        action_cancel = (TextView) view.findViewById(R.id.action_cancel);
        action_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        action_eliminar = (TextView) view.findViewById(R.id.action_eliminar);
        action_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onEliminarImage(mPosition);
                dismiss();
            }
        });

        if (getArguments().getBoolean(ARG_BOOLEAN)) {
            action_cancel.setVisibility(View.GONE);
            action_eliminar.setVisibility(View.GONE);
        }
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnImageDialogFragmentListener) {
                mListener = (OnImageDialogFragmentListener) context;
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            mImage = getArguments().getString(ARG_IMAGE);
            mPosition = getArguments().getInt(ARG_POSITION);
            try {
                Glide.with(getActivity())
                        .load(mImage)
                        .override(800, 800)
                        .into(img_view);
                Bitmap bitmap = BitmapFactory.decodeFile(mImage);
                if (bitmap.getWidth() > bitmap.getHeight()) {
                    img_view.setScaleType(ImageView.ScaleType.FIT_CENTER);
                } else {
                    img_view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnImageDialogFragmentListener {
        void onEliminarImage(int position);
    }

}
