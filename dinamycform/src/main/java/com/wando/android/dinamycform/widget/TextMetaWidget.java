package com.wando.android.dinamycform.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wando.android.dinamycform.generics.DFWidget;

/**
 * Created by Diego on 23/02/2018.
 */

public class TextMetaWidget extends DFWidget {

    transient EditText editText;

    //@Override
    public View generateView(@NonNull final Context context) {
        //String[] textArray = {"One", "Two", "Three", "Four"};
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //Label
        TextView tv = new TextView(context);
        tv.setText(verboseName);
        //tv.setTextSize(10);
        //tv.setTypeface(null, Typeface.BOLD);
        //tv.setTextColor(0xFF555555);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        editText = new EditText(context);
        editText.setText("");
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        editText.setLayoutParams(params2);
        linearLayout.addView(editText);

        return linearLayout;
    }

    //@Override
    public Object getValue() {
        return editText.getText().toString();
    }

    public void setValue(Object object){

    }

}
