package com.wando.android.dinamycform.generics;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.wando.android.dinamycform.DynamicForm;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;
import com.wando.android.dinamycform.exceptions.ValidateException;

import org.json.JSONException;
import org.json.JSONObject;


public abstract class DFFieldType extends DFElement {

    private DFWidget dfWidget;

    private String name;
    private String verboseName;
    private boolean isRequired;
    private String helpText;

    public void isValid() throws ValidateException {
        Log.i("INFO", " name:: " + name + " value :: " + getValue());
        Log.i("INFO", " name:: " + name + " isRequired :: " + isRequired);
        if (isRequired && getValue() == null) {
            throw new ValidateException(String.format("Campo requerido %s", name));
        }
    }

    public abstract Object getValue();

    public abstract void setValue(Object value);

    public View generateView(@NonNull Context context) {
        return dfWidget.generateView(context);
    }

    @Override
    public void load(JSONObject jsonDefault, DynamicForm form) throws JSONException, NoWidgetTypeException, NoElementTypeException {
        super.load(jsonDefault, form);

        JSONObject jsonField = jsonDefault.getJSONObject("field");

        this.getForm().fields.add(this);
        this.dfWidget = this.buildWidget(jsonField);

        this.verboseName = jsonField.getString("verbose_name");
        this.helpText = jsonField.getString("help_text");
        this.isRequired = jsonField.getBoolean("is_required");
        this.name = jsonField.getString("name");
    }

    public DFWidget buildWidget(JSONObject jsonField) throws JSONException, NoWidgetTypeException {
        String type = jsonField.getJSONObject("widget").getString("type");
        DFWidget dfWidget = DFWidget.getWidgetInstance(type);
        dfWidget.load(jsonField);
        return dfWidget;
    }

    public DFWidget getDfWidget() {
        return dfWidget;
    }

    public void setDfWidget(DFWidget dfWidget) {
        this.dfWidget = dfWidget;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerboseName() {
        return verboseName;
    }

    public void setVerboseName(String verboseName) {
        this.verboseName = verboseName;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

}
