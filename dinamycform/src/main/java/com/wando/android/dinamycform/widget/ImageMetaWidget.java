package com.wando.android.dinamycform.widget;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.AlignContent;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.fragment.DFImageFieldViewFragment;
import com.wando.android.dinamycform.generics.DFWidget;
import com.wando.android.dinamycform.util.ImageUtils;
import com.wando.android.dinamycform.util.PermisosUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Diego on 23/02/2018.
 */

public class ImageMetaWidget extends DFWidget {

    transient private Uri mMediaUri;
    transient private File fileTemp;
    transient private List<File> fileList;
    transient private Map<String, String> imageViewPositions = new HashMap<String, String>();
    transient private Map<String, String> imageViewPositionsInver = new HashMap<String, String>();
    transient Context mContext;
    transient private FlexboxLayout flexboxLayout;

    //@Override
    public View generateView(@NonNull final Context context) {
        this.mContext = context;
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //Label
        TextView tv = new TextView(context);
        tv.setText(verboseName);
        //tv.setTextSize(10);
        //tv.setTypeface(null, Typeface.BOLD);
        //tv.setTextColor(0xFF555555);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        flexboxLayout = new FlexboxLayout(context);
        flexboxLayout.setAlignContent(AlignContent.STRETCH);
        flexboxLayout.setAlignItems(AlignItems.STRETCH);
        flexboxLayout.setFlexWrap(FlexWrap.WRAP);
        LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp2.setMargins(0, 10, 0, 10);
        flexboxLayout.setLayoutParams(llp2);
        flexboxLayout.setFlexDirection(FlexDirection.ROW);

        LinearLayout linearLayoutFlex = new LinearLayout(context);
        LinearLayout.LayoutParams llpFlex = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 0, 0, 0);
        linearLayoutFlex.setLayoutParams(llpFlex);
        linearLayoutFlex.setPadding(10, 10, 10, 10);
        linearLayoutFlex.setGravity(Gravity.CENTER);

        linearLayoutFlex.setBackgroundColor(ContextCompat.getColor(mContext.getApplicationContext(), R.color.colorF6F6));
        linearLayoutFlex.setOrientation(LinearLayout.VERTICAL);

        AppCompatImageView appCompatImageView = new AppCompatImageView(context);
        appCompatImageView.setImageResource(R.drawable.ic_camera_alt_black_48dp);
        appCompatImageView.setPadding(4, 4, 4, 4);
        LinearLayout.LayoutParams llpImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //llpImage.width = 80;
        //llpImage.height = 45;
        appCompatImageView.setLayoutParams(llpImage);
        linearLayoutFlex.addView(appCompatImageView);

        AppCompatTextView appCompatTextView = new AppCompatTextView(context);
        appCompatTextView.setText("Foto"); //Agregar
        appCompatTextView.setTextSize(11f);
        appCompatTextView.setGravity(Gravity.CENTER);
        linearLayoutFlex.addView(appCompatTextView);

        linearLayoutFlex.setOnClickListener(view -> {
            takePhoto(context);
        });

        flexboxLayout.addView(linearLayoutFlex);
        linearLayout.addView(flexboxLayout);

        return linearLayout;
    }

    private void takePhoto(Context context) {
        Activity activity = ((Activity) context);
        if (!PermisosUtil.hasCameraPermission(activity)) {
            PermisosUtil.askForCameraPermission(activity);
            return;
        }

        if (!PermisosUtil.hasRealExternalPermission(activity)) {
            PermisosUtil.askForGalleryPermission(activity);
            return;
        }

        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                setFileTemp(ImageUtils.getOutputMediaFile(ImageUtils.IDENTIFY_USER));
                if (getFileTemp() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //BuildConfig.APPLICATION_ID
                        setmMediaUri(FileProvider.getUriForFile(context, "pe.wando.entrega.fileprovider", getFileTemp()));
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } else {
                        setmMediaUri(Uri.fromFile(getFileTemp()));
                    }
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, getmMediaUri());
                    intent.putExtra("return-data", true);
                    ImageUtils.NAME_PICTURE_DFFIELD = name; //TODO esto se puede corregir usando wandosessión
                    activity.startActivityForResult(intent, ImageUtils.REQUEST_CODE_CAPTURE);
                } else {
                    //throw new RuntimeException("Error external store - IMAGE");
                    Toast.makeText(context, "Error external store - IMAGE", Toast.LENGTH_LONG).show();
                }
            } else {
                //throw new RuntimeException("Error external store - IMAGE");
                Toast.makeText(context, "Error external store - IMAGE", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public FlexboxLayout getFlexboxLayout() {
        return flexboxLayout;
    }

    public Object getValue() {
        return getFileList();
    }

    public void setValue(Object object) {
    }

    public void showImageDialog(String image, int position) {
        FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
        DFImageFieldViewFragment newFragment = DFImageFieldViewFragment.newInstance(image, position, false);
        newFragment.show(fragmentManager, "dialog");
        /*if (mIsLargeLayout) {
            // The device is using a large layout, so show the fragment as a dialog
            newFragment.show(fragmentManager, "dialog");
        } else {
            // The device is smaller, so show the fragment fullscreen
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            // For a little polish, specify a transition animation
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            // To make it fullscreen, use the 'content' root view as the container
            // for the fragment, which is always the root view for the activity
            transaction.add(android.R.id.content, newFragment)
                    .addToBackStack(null).commit();
        }*/
    }

    public void addImageFlexBox() {

        if (fileList == null) fileList = new ArrayList<>();

        AppCompatImageView appCompatImageView = new AppCompatImageView(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        appCompatImageView.setLayoutParams(params);

        getFlexboxLayout().addView(appCompatImageView, getFileList().size() + 1);

        Glide.with(mContext)
                .load(getFileTemp())
                .override(170, 200)
                .centerCrop()
                .into(appCompatImageView);

        getFileList().add(getFileTemp());

        imageViewPositions.put(appCompatImageView.getTag().toString(), getFileTemp().getAbsolutePath());
        imageViewPositionsInver.put(getFileTemp().getAbsolutePath(), appCompatImageView.getTag().toString());

        appCompatImageView.setOnClickListener(view -> {
            try {
                //------------------------------------
                //Log.i("INFO","view.getTag " + view.getTag().toString());
                ImageUtils.NAME_PICTURE_DFFIELD = name;
                String image = imageViewPositions.get(view.getTag().toString());
                //Log.i("INFO","image " + image);
                showImageDialog(image, getIndexFileList(image));
                //------------------------------------
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        });
    }

    public void removeImageFlexBox(int position) {
        try {
            //----------------------------------------------------------------
            int indexFlex = (position + 1);
            //Log.i("INFO"," position "+ position);
            //Log.i("INFO"," indexFlex "+ indexFlex);
            String keyPosition = imageViewPositionsInver.get(getPathFileList(position));
            String valuePosition = imageViewPositions.get(keyPosition);
            //Log.i("INFO","imageViewPositions ant " + imageViewPositions.toString() );
            //----------------------------------------------------------------
            getFlexboxLayout().removeView(getFlexboxLayout().getFlexItemAt(indexFlex));
            getFlexboxLayout().refreshDrawableState();
            //----------------------------------------------------------------
            imageViewPositions.remove(keyPosition);
            imageViewPositionsInver.remove(valuePosition);
            //----------------------------------------------------------------
            fileList.remove(position);
            //Log.i("INFO","imageViewPositions des " + imageViewPositions.toString() );
            //----------------------------------------------------------------
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Uri getmMediaUri() {
        return mMediaUri;
    }

    public void setmMediaUri(Uri mMediaUri) {
        this.mMediaUri = mMediaUri;
    }

    public File getFileTemp() {
        return fileTemp;
    }

    public void setFileTemp(File fileTemp) {
        this.fileTemp = fileTemp;
    }

    public int getIndexFileList(String nameFile) {
        int index = -1;
        for (int x = 0; x < fileList.size(); x++) {
            if (fileList.get(x).getAbsolutePath().equalsIgnoreCase(nameFile)) {
                index = x;
                break;
            }
        }
        return index;
    }

    public List<File> getFileList() {
        return fileList;
    }

    public String getPathFileList(int position) {
        return fileList.get(position).getAbsolutePath();
    }

}