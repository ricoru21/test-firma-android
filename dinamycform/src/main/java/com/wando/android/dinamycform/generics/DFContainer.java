package com.wando.android.dinamycform.generics;

import com.wando.android.dinamycform.DynamicForm;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public abstract class DFContainer extends DFElement {

    private List<DFElement> elements = new ArrayList<>();

    @Override
    public void load(JSONObject jsonDefault, DynamicForm form) throws JSONException, NoWidgetTypeException, NoElementTypeException {
        super.load(jsonDefault, form);

        JSONArray jrElementos = jsonDefault.getJSONArray("elements"); //fields
        for (int x = 0; x < jrElementos.length(); x++) {
            JSONObject jsonObject = jrElementos.getJSONObject(x);
            String type = jsonObject.getString("type");
            DFElement element =  DFElement.getElementInstance(type);
            getElements().add(element);
            element.load(jsonObject, form);
        }
    }

    public List<DFElement> getElements() {
        return elements;
    }

    public void setElements(List<DFElement> elements) {
        this.elements = elements;
    }
}
