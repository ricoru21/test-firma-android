package com.wando.android.dinamycform;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;
import com.wando.android.dinamycform.exceptions.ValidateException;
import com.wando.android.dinamycform.generics.DFElement;
import com.wando.android.dinamycform.generics.DFFieldType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Diego on 23/02/2018.
 */

public class DynamicForm {

    public List<DFFieldType> fields = new ArrayList<>();
    private DFElement root;
    private String baseUrlImage = "";

    public DynamicForm(String format) throws JSONException, NoElementTypeException, NoWidgetTypeException {
        //obtener la lista de fields
        JSONObject jsonObj = new JSONObject(format);
        JSONObject jsonDefault = jsonObj.getJSONObject("default");

        // title = jsonDefault.getString("title");
        String type = jsonDefault.getString("type");
        this.root = DFElement.getElementInstance(type);
        this.root.load(jsonDefault, this);
    }

    public void addToActivity(Activity a) {
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        a.addContentView(this.root.generateView(a), llp);
    }

    public void addToActivityReadOnly(Activity a, ViewGroup view) {
        LayoutInflater inflater = LayoutInflater.from(a);
        LinearLayout content = (LinearLayout) inflater.inflate(R.layout.custom_content_linearlayout, view, false);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        //LinearLayout contentRoot = new LinearLayout(a);
        //contentRoot.setOrientation(LinearLayout.VERTICAL);
        //contentRoot.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.addView(this.root.generateReadOnlyView(a, content), llp);
        //a.addContentView(view, llp);
    }

    public Map<String, Object> getValues() throws ValidateException {
        HashMap<String, Object> m = new HashMap<>();
        try {
            for (DFFieldType f : fields) {
                f.isValid();
                m.put(f.getName(), f.getValue());
            }
        } catch (ValidateException e) {
            e.printStackTrace();
            throw e; // se lanza para que sea procesado por el activity.
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m;
    }

    public void setValues(Map<String, Object> values) {
        for (Map.Entry<String, Object> item : values.entrySet()) {
            getField(item.getKey()).setValue(item.getValue());
        }
    }

    public DFFieldType getField(String name) {
        for (DFFieldType item : fields) {
            if (name.equalsIgnoreCase(item.getName())) {
                return item;
            }
        }
        return null;
    }

    public String getBaseUrlImage() {
        return baseUrlImage;
    }

    public void setBaseUrlImage(String baseUrlImage) {
        this.baseUrlImage = baseUrlImage;
    }

}

