package com.wando.android.dinamycform.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.signature.views.SignaturePad;

import java.io.ByteArrayOutputStream;

public class DFSignatureFieldFragment extends DialogFragment {

    private static final String ARG_NAME_FIELDTYPE = "ARG_NAME_FIELDTYPE";
    private static final String ARG_DATA = "ARG_DATA";
    private static OnSignatureDialogFragmentListener mListener;

    SignaturePad mSignaturePad;

    public DFSignatureFieldFragment() {
        // Required empty public constructor
    }

    public static DFSignatureFieldFragment newInstance(String name, byte[] data) {
        DFSignatureFieldFragment fragment = new DFSignatureFieldFragment();
        Bundle args = new Bundle();
        args.putByteArray(ARG_DATA, data);
        args.putString(ARG_NAME_FIELDTYPE, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signature_dialog, container, false);
        AppCompatButton clear_signature = (AppCompatButton) view.findViewById(R.id.btn_clear_signature);
        AppCompatButton save_signature = (AppCompatButton) view.findViewById(R.id.btn_save_signature);
        mSignaturePad = (SignaturePad) view.findViewById(R.id.signature_pad);

        clear_signature.setOnClickListener(view1 -> {
            mSignaturePad.clear();
            if (getArguments() != null) {
                mListener.onClearSignature(getArguments().getString(ARG_NAME_FIELDTYPE));
            }
        });

        save_signature.setOnClickListener(view1 -> {
            saveSignature();
            dismiss();
        });

        initSignaturePad();
        /*img_view = (ImageView) view.findViewById(R.id.img_view);
        action_cancel = (TextView) view.findViewById(R.id.action_cancel);
        action_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        action_eliminar = (TextView) view.findViewById(R.id.action_eliminar);
        action_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onEliminarImage(mPosition);
                dismiss();
            }
        });*/
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnSignatureDialogFragmentListener) {
                mListener = (OnSignatureDialogFragmentListener) context;
            }
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement DFSignatureFieldFragment");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;
    }

    public interface OnSignatureDialogFragmentListener {
        void onSaveSignature(String nameFieldType, byte[] data);

        void onClearSignature(String nameFieldType);

        void onCaptureException(Throwable throwable);
    }

    private void initSignaturePad() {
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
            }
        });

        if (getArguments().getByteArray(ARG_DATA) != null) {
            try {
                //byte[] firma = Base64.decode(session.values.currentFirmaElectronica.Firma, Base64.DEFAULT);
                //Log.i("INFO","len "+ firma.length);
                byte[] data = getArguments().getByteArray(ARG_DATA);
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                mSignaturePad.setSignatureBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }

        if (mListener == null) {
            mListener = (OnSignatureDialogFragmentListener) getActivity();
        }

        if (mListener == null) {
            mListener = (OnSignatureDialogFragmentListener) getTargetFragment();
        }
    }

    private void saveSignature() {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mSignaturePad.getSignatureBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] firma = stream.toByteArray();
            mListener.onSaveSignature(getArguments().getString(ARG_NAME_FIELDTYPE), firma);
            stream.close();
            dismiss();
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            if(mListener!=null) mListener.onCaptureException(e);
            dismiss();
        }
    }

}
