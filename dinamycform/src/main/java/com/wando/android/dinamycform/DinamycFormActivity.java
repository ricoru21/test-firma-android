package com.wando.android.dinamycform;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wando.android.dinamycform.activity.DFSignatureFieldActivity;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;
import com.wando.android.dinamycform.exceptions.ValidateException;
import com.wando.android.dinamycform.fragment.DFImageFieldViewFragment;
import com.wando.android.dinamycform.generics.DFFieldType;
import com.wando.android.dinamycform.util.ImageUtils;
import com.wando.android.dinamycform.util.PermisosUtil;
import com.wando.android.dinamycform.widget.ImageMetaWidget;
import com.wando.android.dinamycform.widget.SignatureMetaWidget;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class DinamycFormActivity extends AppCompatActivity implements
        DFImageFieldViewFragment.OnImageDialogFragmentListener {

    public static int REQUEST_CODE_DINAMYC = 1000;
    public static String INTENT_FORMAT_FORMULARIO = "format_formulario";
    public static String INTENT_EXCEPTION = "format_formulario";
    public static String INTENT_EXTRA_FORMULARIO = "extra_formulario";
    public static String INTENT_EXTRA_ID_USER = "id_user_formulario";

    DynamicForm df;

    public static void strartDinamycFormActivity(Activity activity, Bundle bundle, int REQUEST_CODE) {
        try {
            Intent intent = new Intent(activity, DinamycFormActivity.class);
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_df);

        init();
        initToolbar();
    }

    private void initToolbar() {
        getSupportActionBar().setTitle("Formulario");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        String format = "";
        //"{\"default\": {\"type\": \"FieldSetContainer\",\"title\": \"Login\",\"elements\": [{\"type\": \"FieldContainer\",\"field\": {\"type\": \"TextField\",\"name\": \"username\",\"verbose_name\": \"Usuario\",\"help_text\": \"Ingrese su usuario\",\"required\": true,\"widget\": {\"type\": \"TextMetaWidget\",\"icon\": \"fa-user\"}}},{\"type\": \"FieldContainer\",\"field\": {\"type\": \"TextField\",\"name\": \"password\",\"verbose_name\": \"Contraseña\",\"help_text\": \"Ingrese su contraseña\",\"required\": true,\"widget\": {\"type\": \"PasswordMetaWidget\",\"icon\": \"fa-secure\"}}},{\"type\": \"FieldContainer\",\"field\": {\"type\": \"ImageField\",\"name\": \"picture\",\"verbose_name\": \"Foto\",\"help_text\": \"Toma unas fotos\",\"required\": true,\"widget\": {}}}]}}";
        //{"default": {"elements": [{"field": {"name": "password", "choices": null, "is_required": false, "verbose_name": "Contrase\u00f1a", "max_length": null, "help_text": null, "widget": {"style": null}}, "type": "TextFieldType"}, {"field": {"name": "username", "choices": null, "is_required": false, "verbose_name": "Usuario", "max_length": null, "help_text": null, "widget": {"style": null}}, "type": "TextFieldType"}], "title": "Form Entregado", "type": "MetaFieldSetContainer"}}
        if (getIntent().getExtras() != null) {
            format = getIntent().getStringExtra(INTENT_FORMAT_FORMULARIO);
            ImageUtils.IDENTIFY_USER = getIntent().getIntExtra(INTENT_EXTRA_ID_USER, 0);

            try {
                df = new DynamicForm(format);
            } catch (JSONException e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } catch (NoElementTypeException e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } catch (NoWidgetTypeException e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            if (df != null) df.addToActivity(this);
        }

        if (!PermisosUtil.hasRealExternalPermission(DinamycFormActivity.this)) {
            PermisosUtil.askForGalleryPermission(DinamycFormActivity.this);
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }
        if (id == R.id.item_go) {
            Map respuesta = new HashMap<>();
            try {
                respuesta = df.getValues();
            } catch (ValidateException e) {
                Toast.makeText(this, "Es necesario completar todo los campos", Toast.LENGTH_SHORT).show();
            } finally {
                Gson g = new Gson();
                Log.i("INFO","g.toJson(respuesta) "+ g.toJson(respuesta));
                Intent intentResult = new Intent();
                intentResult.putExtra(INTENT_EXTRA_FORMULARIO, g.toJson(respuesta));
                setResult(RESULT_OK, intentResult);
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtils.REQUEST_CODE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    DFFieldType dfField = df.getField(ImageUtils.NAME_PICTURE_DFFIELD);
                    ImageMetaWidget dfWidget = (ImageMetaWidget) dfField.getDfWidget();
                    dfWidget.addImageFlexBox();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == DFSignatureFieldActivity.REQUEST_CODE_CAPTURE) {
            if (data != null) {
                if (resultCode == RESULT_OK) {
                    if (data.getBooleanExtra(DFSignatureFieldActivity.INTENT_NOT_SAVE_FIRMA, false)) {
                        byte[] firma = data.getByteArrayExtra(DFSignatureFieldActivity.INTENT_DATA);
                        DFFieldType dfField = df.getField(data.getStringExtra(DFSignatureFieldActivity.INTENT_NAME_FIELDTYPE));
                        SignatureMetaWidget dfWidget = (SignatureMetaWidget) dfField.getDfWidget();
                        dfWidget.addViewSignature(firma);
                    } else {
                        DFFieldType dfField = df.getField(DFSignatureFieldActivity.INTENT_NAME_FIELDTYPE);
                        SignatureMetaWidget dfWidget = (SignatureMetaWidget) dfField.getDfWidget();
                        dfWidget.removeViewSignature();
                    }
                } else {
                    Intent intentResult = new Intent();
                    intentResult.putExtra(INTENT_EXCEPTION, data.getStringExtra(DFSignatureFieldActivity.INTENT_EXCEPTION));
                    setResult(RESULT_CANCELED, intentResult);
                    finish();
                }
            }
        }

    }

    @Override
    public void onEliminarImage(int position) {
        DFFieldType dfField = df.getField(ImageUtils.NAME_PICTURE_DFFIELD);
        ImageMetaWidget dfWidget = (ImageMetaWidget) dfField.getDfWidget();
        dfWidget.removeImageFlexBox(position);
    }

}
