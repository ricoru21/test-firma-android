package com.wando.android.dinamycform.elements;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.AlignContent;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.fragment.DFImageFieldViewFragment;
import com.wando.android.dinamycform.generics.DFFieldType;
import com.wando.android.dinamycform.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageFieldType extends DFFieldType {

    transient Context mContext;
    transient List<String> images;
    transient private Map<String, String> imageViewPositions = new HashMap<String, String>();
    transient private FlexboxLayout flexboxLayout;

    @Override
    public Object getValue() {
        return getDfWidget().getValue();
    }

    @Override
    public void setValue(Object value) {
        images = (ArrayList<String>) value;
    }

    @Override
    public View generateReadOnlyView(@NonNull Context context, ViewGroup root) {
        this.mContext = context;

        LinearLayout.LayoutParams llp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView tv = new TextView(context);
        tv.setText(StringUtil.strUpperFirstLetter(this.getVerboseName()));
        tv.setTextSize(15f);
        tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.gravity = Gravity.CENTER_VERTICAL;
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        if (images.size() > 0) {
            flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setAlignContent(AlignContent.STRETCH);
            flexboxLayout.setAlignItems(AlignItems.STRETCH);
            flexboxLayout.setFlexWrap(FlexWrap.WRAP);
            LinearLayout.LayoutParams llp2 =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            llp2.setMargins(0, 10, 0, 10);
            flexboxLayout.setLayoutParams(llp2);
            flexboxLayout.setFlexDirection(FlexDirection.ROW);

            LinearLayout linearLayoutFlex = new LinearLayout(context);
            LinearLayout.LayoutParams llpFlex =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(0, 0, 0, 0);
            linearLayoutFlex.setLayoutParams(llpFlex);
            linearLayoutFlex.setPadding(10, 10, 10, 10);
            linearLayoutFlex.setGravity(Gravity.CENTER);
            linearLayoutFlex.setBackgroundColor(
                    ContextCompat.getColor(mContext.getApplicationContext(), R.color.colorF6F6));
            linearLayoutFlex.setOrientation(LinearLayout.VERTICAL);

            flexboxLayout.addView(linearLayoutFlex);
            linearLayout.addView(flexboxLayout);

            for (int x = 0; x < images.size(); x++) {
                String url = images.get(x);
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = getForm().getBaseUrlImage()
                            .split("api/v1/")[0] + "media/fotos/" + images.get(x);
                    addImageFlexBox(url, x);
                } else {
                    addImageFlexBox(url, x);
                }
            }
        }

        View divider = new View(mContext);
        divider.setBackgroundResource(R.color.colorF6F6);
        LinearLayout.LayoutParams dividerParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        dividerParams.height = 1;
        dividerParams.topMargin = 20;
        dividerParams.bottomMargin = 20;
        divider.setLayoutParams(dividerParams);
        linearLayout.addView(divider);

        if (images == null) {
            linearLayout.setVisibility(View.GONE);
        }

        return linearLayout;
    }

    private void addImageFlexBox(String url, int positon) {
        AppCompatImageView appCompatImageView = new AppCompatImageView(mContext);
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        appCompatImageView.setLayoutParams(params);

        getFlexboxLayout().addView(appCompatImageView, positon);

        Glide.with(mContext)
                .load(url)
                .override(170, 200)
                .centerCrop()
                .into(appCompatImageView);

        imageViewPositions.put(appCompatImageView.getTag().toString(), url);
        appCompatImageView.setOnClickListener(view -> {
            try {
                //------------------------------------
                String image = imageViewPositions.get(view.getTag().toString());
                showImageDialog(image, positon);
                //------------------------------------
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        });
    }

    private void showImageDialog(String image, int position) {
        FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
        DFImageFieldViewFragment newFragment
                = DFImageFieldViewFragment.newInstance(image, position, true);
        newFragment.show(fragmentManager, "dialog");
    }

    private FlexboxLayout getFlexboxLayout() {
        return flexboxLayout;
    }

}
