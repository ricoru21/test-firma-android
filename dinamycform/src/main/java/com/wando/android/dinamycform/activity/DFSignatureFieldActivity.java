package com.wando.android.dinamycform.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.signature.views.SignaturePad;

import java.io.ByteArrayOutputStream;

public class DFSignatureFieldActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_CAPTURE = 3000;
    public static final String INTENT_NAME_FIELDTYPE = "INTENT_NAME_FIELDTYPE";
    public static final String INTENT_DATA = "INTENT_DATA";
    public static final String INTENT_EXCEPTION = "INTENT_EXCEPTION";
    public static final String INTENT_NOT_SAVE_FIRMA = "INTENT_NOT_SAVE_FIRMA";

    private SignaturePad mSignaturePad;
    private boolean isSaveCapture = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dfsignature_field);

        init();
    }

    private void init() {
        AppCompatButton clear_signature = findViewById(R.id.btn_clear_signature);
        AppCompatButton save_signature = findViewById(R.id.btn_save_signature);
        mSignaturePad = findViewById(R.id.signature_pad);

        clear_signature.setOnClickListener(view1 -> {
            mSignaturePad.clear();
            isSaveCapture = false;
        });

        save_signature.setOnClickListener(view1 -> {
            isSaveCapture = true;
            saveSignature();
        });

        initSignaturePad();
    }

    private void initSignaturePad() {
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
            }
        });

        if (getIntent().getByteArrayExtra(INTENT_DATA) != null) {
            try {
                byte[] data = getIntent().getByteArrayExtra(INTENT_DATA);
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                mSignaturePad.setSignatureBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(DFSignatureFieldActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                Intent intentResult = new Intent();
                String message = e.getStackTrace() != null ? new Gson().toJson(e.getStackTrace()) : e.getMessage();
                intentResult.putExtra(INTENT_EXCEPTION, message);
                setResult(RESULT_CANCELED, intentResult);
                finish();
            }
        }
    }

    private void saveSignature() {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mSignaturePad.getSignatureBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] firma = stream.toByteArray();
            stream.close();

            Intent intentResult = new Intent();
            intentResult.putExtra(INTENT_NAME_FIELDTYPE, getIntent().getStringExtra(INTENT_NAME_FIELDTYPE));
            intentResult.putExtra(INTENT_DATA, firma);
            intentResult.putExtra(INTENT_NOT_SAVE_FIRMA, isSaveCapture);
            setResult(RESULT_OK, intentResult);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(DFSignatureFieldActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            Intent intentResult = new Intent();
            String message = e.getStackTrace() != null ? new Gson().toJson(e.getStackTrace()) : e.getMessage();
            intentResult.putExtra(INTENT_NOT_SAVE_FIRMA, isSaveCapture);
            intentResult.putExtra(INTENT_EXCEPTION, message);
            setResult(RESULT_CANCELED, intentResult);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isSaveCapture = false;
        Intent intentResult = new Intent();
        intentResult.putExtra(INTENT_NAME_FIELDTYPE, getIntent().getStringExtra(INTENT_NAME_FIELDTYPE));
        intentResult.putExtra(INTENT_NOT_SAVE_FIRMA, isSaveCapture);
        setResult(RESULT_OK, intentResult);
        finish();
    }
}
