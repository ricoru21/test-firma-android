package com.wando.android.dinamycform.util;

public class StringUtil {

    public static String strUpperFirstLetter(String texto) {
        StringBuilder sb = new StringBuilder(texto);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

}
