package com.wando.android.dinamycform.elements;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.wando.android.dinamycform.DynamicForm;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;
import com.wando.android.dinamycform.generics.DFContainer;
import com.wando.android.dinamycform.generics.DFElement;

import org.json.JSONException;
import org.json.JSONObject;


public class MetaFieldSetContainer extends DFContainer {

    private String title;
    private Context mContext;

    @Override
    public View generateView(@NonNull Context context) {
        this.mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        ScrollView mainView = (ScrollView) inflater.inflate(R.layout.activity_main_df,
                null, false);

        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        for (DFElement element : this.getElements()) {
            layout.addView(element.generateView(this.mContext));
        }
        mainView.addView(layout);
        return mainView;
    }

    @Override
    public View generateReadOnlyView(@NonNull Context context, ViewGroup mainView) {
        this.mContext = context;
        LinearLayout content = new LinearLayout(mainView.getContext());
        content.setOrientation(LinearLayout.VERTICAL);
        content.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        for (DFElement element : this.getElements()) {
            content.addView(element.generateReadOnlyView(mainView.getContext(), content));
        }
        mainView.addView(content);
        return mainView;
    }

    @Override
    public void load(JSONObject jsonDefault, DynamicForm form) throws JSONException,
            NoElementTypeException, NoWidgetTypeException {
        super.load(jsonDefault, form);
        this.title = jsonDefault.getString("title");
    }

}
