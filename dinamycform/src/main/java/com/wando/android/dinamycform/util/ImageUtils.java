package com.wando.android.dinamycform.util;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hoanglam on 7/31/16.
 */
public class ImageUtils {

    private static final String TAG = ImageUtils.class.getSimpleName();
    public static final int REQUEST_CODE_CAPTURE = 2000;

    public static String NAME_PICTURE_DFFIELD = "";
    public static int IDENTIFY_USER = 0;
    //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Emotiions");
    public static String IMAGE_DIRECTORY_NAME = "dynamicforms";//Environment.DIRECTORY_PICTURES;// "Camera";

    public static File getOutputMediaFile(int identify_user) {
        //Environment.DIRECTORY_PICTURES
        // External sdcard location //getExternalStoragePublicDirectory
        Log.i(TAG, "Environment.DIRECTORY_PICTURES " + Environment.DIRECTORY_PICTURES);

        Log.i(TAG, "Environment.getExternalStorageDirectory() "+ Environment.getExternalStorageDirectory());

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        Log.i(TAG, "mediaStorageDir:exists " + mediaStorageDir.exists());
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.i("ImageUtils", "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Log.i(TAG, "mediaStorageDir:exists " + mediaStorageDir.exists());
        File mediaFile = null;
        try {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "E_" + identify_user + "_" + timeStamp + ".jpg");
            if (!mediaFile.exists()) {
                mediaFile.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mediaFile;
    }

    /**
     * Writes the specified byte[] to the specified File path.
     *
     * @param file  the File
     * @param bytes The byte[] of data to write to the File.
     *              File.
     */
    public static File bytesToFile(File file, byte[] bytes) throws IOException {
        /*File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.i("ImageUtils", "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }*/
        Log.i(TAG, "file.path " + file.getAbsolutePath());

        BufferedOutputStream bos = null;
        try {
            // Create a media file name
            FileOutputStream fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
        } finally {
            if (bos != null) {
                try {
                    //flush and close the BufferedOutputStream
                    bos.flush();
                    bos.close();
                } catch (Exception e) {
                }
            }
        }
        return file;
    }

    public static void saveBitmapPhoto(File file, Bitmap bitmap) {
        FileOutputStream fileOutputStream = null;
        try {
            ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getByteCount());
            bitmap.copyPixelsToBuffer(byteBuffer);
            byte[] bytes = byteBuffer.array();
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    //fileOutputStream.flush();
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /*FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }


}
