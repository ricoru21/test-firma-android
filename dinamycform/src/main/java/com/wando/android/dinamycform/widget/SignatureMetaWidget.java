package com.wando.android.dinamycform.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wando.android.dinamycform.R;
import com.wando.android.dinamycform.activity.DFSignatureFieldActivity;
import com.wando.android.dinamycform.generics.DFWidget;
import com.wando.android.dinamycform.util.ImageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignatureMetaWidget extends DFWidget {

    transient private Context mContext;
    transient private byte[] mData = null;
    transient private List<File> files = null;
    transient private TextView messageInit;
    transient private ImageView imageSignature;

    @Override
    public View generateView(@NonNull Context context) {
        this.mContext = context;

        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.content_signature, null, false);

        TextView tv = (TextView) linearLayout.findViewById(R.id.txt_name_signature);
        tv.setText(verboseName);

        imageSignature = (ImageView) linearLayout.findViewById(R.id.img_click_signature);
        imageSignature.setOnClickListener(view -> {
            irSignatureComponent();
        });

        messageInit = (TextView) linearLayout.findViewById(R.id.txt_click_signature);
        messageInit.setOnClickListener(view -> {
            irSignatureComponent();
        });

        imageSignature.setVisibility(View.GONE);
        messageInit.setVisibility(View.VISIBLE);

        return linearLayout;
    }

    @Override
    public Object getValue() {
        try {
            if (mData != null) {
                files = new ArrayList<>();
                files.add(ImageUtils.bytesToFile(ImageUtils.getOutputMediaFile(ImageUtils.IDENTIFY_USER), mData));
            }
            Log.i("SignatureMetaWidget", "files.size "+ files.size());
        } catch (Exception e) {
            e.printStackTrace();
            return files;
        }
        return  files;
    }

    @Override
    public void setValue(Object object) {
    }

    private void irSignatureComponent() {
        Intent intent = new Intent(mContext, DFSignatureFieldActivity.class);
        intent.putExtra(DFSignatureFieldActivity.INTENT_NAME_FIELDTYPE, name);
        intent.putExtra(DFSignatureFieldActivity.INTENT_DATA, mData);
        ((Activity) mContext).startActivityForResult(intent, DFSignatureFieldActivity.REQUEST_CODE_CAPTURE);
        /*FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
        DFSignatureFieldFragment newFragment = DFSignatureFieldFragment.newInstance(name, mData);
        newFragment.show(fragmentManager, "dialog");*/
    }

    public void addViewSignature(byte[] data) {
        this.mData = data;
        Bitmap bitmap = BitmapFactory.decodeByteArray(mData, 0, mData.length);
        if (bitmap.getWidth() > bitmap.getHeight()) {
            //imageSignature.setRotation(180f);
            imageSignature.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            //imageSignature.setRotation(-270f);
            imageSignature.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        Glide.with(mContext)
                .load(mData)
                .override(getDimensions().get("width"), getDimensions().get("height") / 2)
                .into(imageSignature);

        imageSignature.setVisibility(View.VISIBLE);
        messageInit.setVisibility(View.GONE);
    }

    public void removeViewSignature() {
        this.mData = null;
        imageSignature.setImageDrawable(null);
        imageSignature.setVisibility(View.GONE);
        messageInit.setVisibility(View.VISIBLE);
    }

    private Map<String, Integer> getDimensions() {
        Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Map<String, Integer> dimensions = new HashMap<>();
        dimensions.put("height", height);
        dimensions.put("width", width);
        return dimensions;
    }

}
