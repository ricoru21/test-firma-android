package com.wando.android.dinamycform.generics;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.wando.android.dinamycform.BuildConfig;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Diego on 23/02/2018.
 */

public abstract class DFWidget {

    protected String name;
    protected String verboseName;
    protected boolean isRequired;
    protected String helpText;

    private static final String WIDGET_PACKAGE = BuildConfig.APPLICATION_ID+".widget";

    public abstract View generateView(@NonNull Context context);

    public void load(JSONObject jsonField) throws JSONException {
        this.verboseName = jsonField.getString("verbose_name");
        this.helpText = jsonField.getString("help_text");
        this.isRequired = jsonField.getBoolean("is_required");
        this.name = jsonField.getString("name");
    }

    public abstract Object getValue();

    public abstract void setValue(Object object);

    public static DFWidget getWidgetInstance(String widget) throws NoWidgetTypeException {
        try {
            return (DFWidget) Class.forName(String.format("%s.%s", WIDGET_PACKAGE, widget)).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoWidgetTypeException("No se pudo cargar el widget " + widget);
        }
    }
}

