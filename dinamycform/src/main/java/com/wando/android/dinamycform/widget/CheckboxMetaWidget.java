package com.wando.android.dinamycform.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wando.android.dinamycform.generics.DFWidget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 23/02/2018.
 */

public class CheckboxMetaWidget extends DFWidget {

    transient private List<ItemComboField> itemOptions = new ArrayList<>();
    transient private List<ItemComboField> selectItems = new ArrayList<>();
    transient Context mContext;
    transient private String formatItems = ""; //"[{\"key\":\"1\",\"value\":\"option 1\"},{\"key\":\"2\",\"value\":\"option 2\"},{\"key\":\"3\",\"value\":\"option 3\"}]";


    public View generateView(@NonNull final Context context) {
        this.mContext = context;
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 10, 0, 10);
        linearLayout.setLayoutParams(llp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //Label
        TextView tv = new TextView(context);
        tv.setText(verboseName);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(params);
        linearLayout.addView(tv);

        if (formatItems != null) {
            try {
                JSONArray jsonArray = new JSONArray(formatItems);
                for (int x = 0; x < jsonArray.length(); x++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(x);
                    ItemComboField item = new Gson().fromJson(jsonObject.toString(), ItemComboField.class);
                    itemOptions.add(item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LinearLayout linearLayout_checkbox = new LinearLayout(context);
        LinearLayout.LayoutParams llpCheckbox = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llpCheckbox.setMargins(0, 5, 0, 5);
        linearLayout_checkbox.setLayoutParams(llpCheckbox);
        linearLayout_checkbox.setOrientation(LinearLayout.VERTICAL);

        for (ItemComboField item : itemOptions) {
            CheckBox checkItem = new CheckBox(context);
            checkItem.setPadding(4, 4, 4, 4);
            checkItem.setText(item.value);
            checkItem.setTag(item.key);
            LinearLayout.LayoutParams llpCheck = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            checkItem.setLayoutParams(llpCheck);

            checkItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    itemSelected(compoundButton.getTag().toString());
                }
            });

            linearLayout.addView(checkItem);
        }
        linearLayout.addView(linearLayout_checkbox);

        return linearLayout;
    }

    public Object getValue() {
        return getKeyItemSelected();
    }

    public void setValue(Object object){

    }

    public List<ItemComboField> getItemSelected() {
        return selectItems;
    }

    private List<String> getKeyItemSelected() {
        List<String> keys = new ArrayList<>();
        for (int x = 0; x < selectItems.size(); x++) {
            keys.add(selectItems.get(x).key);
        }
        return keys;
    }

    private void itemSelected(String key) {
        ItemComboField item = null;
        for (int x = 0; x < itemOptions.size(); x++) {
            if (itemOptions.get(x).key.equals(key)) {
                item = itemOptions.get(x);
                break;
            }
        }
        if (!selectItems.contains(item)) {
            selectItems.add(item);
        } else {
            selectItems.remove(item);
        }
    }

    public class ItemComboField {
        private String key;
        public String value;

        @Override
        public String toString() {
            return value;
        }
    }

}