package com.wando.android.dinamycform.generics;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.wando.android.dinamycform.BuildConfig;
import com.wando.android.dinamycform.DynamicForm;
import com.wando.android.dinamycform.exceptions.NoElementTypeException;
import com.wando.android.dinamycform.exceptions.NoWidgetTypeException;

import org.json.JSONException;
import org.json.JSONObject;


public abstract class DFElement {

    private static final String ELEMENTS_PACKAGE = BuildConfig.APPLICATION_ID + ".elements";
    private DynamicForm form;

    public abstract View generateView(@NonNull Context context);

    public abstract View generateReadOnlyView(@NonNull Context context, ViewGroup root);

    public void load(JSONObject jsonDefault, DynamicForm form) throws JSONException, NoWidgetTypeException, NoElementTypeException {
        this.setForm(form);
    }

    public static DFElement getElementInstance(String type) throws NoElementTypeException {
        try {
            return (DFElement) Class.forName(String.format("%s.%s", ELEMENTS_PACKAGE, type)).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoElementTypeException("No se pudo cargar el widget " + type);
        }
    }

    public DynamicForm getForm() {
        return form;
    }

    public void setForm(DynamicForm form) {
        this.form = form;
    }

}


