package com.richardoruna.testfirma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.wando.android.dinamycform.DinamycFormActivity;

public class MainActivity extends AppCompatActivity {

    TextView textViewError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle argument = new Bundle();
        int user_test = 10;
        String format = "{\"default\": {\"elements\": [{\"type\": \"ImageFieldType\", \"order\": null, \"field\": {\"is_required\": true, \"widget\": {\"type\": \"SignatureMetaWidget\", \"style\": null}, \"name\": \"firma\", \"verbose_name\": \"Firma\", \"help_text\": null}}], \"type\": \"MetaFieldSetContainer\", \"title\": \"Formulario Entregado\"}}";
        argument.
                putString(DinamycFormActivity.INTENT_FORMAT_FORMULARIO,
                        format);
        argument.
                putInt(DinamycFormActivity.INTENT_EXTRA_ID_USER,
                        user_test);
        DinamycFormActivity.
                strartDinamycFormActivity(MainActivity.this,
                        argument, DinamycFormActivity.REQUEST_CODE_DINAMYC);


        textViewError = findViewById(R.id.txt_error_capturado);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DinamycFormActivity.REQUEST_CODE_DINAMYC) {
            if (data != null) {
                if (data.getStringExtra(DinamycFormActivity.INTENT_EXCEPTION) != null) {
                    textViewError.setText(data.getStringExtra(DinamycFormActivity.INTENT_EXCEPTION));
                }
            }
        }
    }

}
